import Card from "../Card/Card";
import {cards} from "../cards";

export class Deck {

    private _deck: Card[] = [];
    private _cardsInPlay: Card[] = [];

    constructor() {
        cards.forEach( card => {
            this._deck.push(Card.createFromJSON(card.rank, card.value, card.suit))
        });
    }

    get deck() {
        return this._deck;
    }

    get cardsInPlay() {
        return this._cardsInPlay;
    }

    public deal(numCardsToDeal) {
        let cardsToDeal: Card[] = [];

        for(let i = 0; i < numCardsToDeal; i++)
            cardsToDeal.push(this.draw());

        return cardsToDeal;
    }

    public draw() {
        let card = this.deck.shift();
        this._cardsInPlay.push(card);
        return card;
    }

    public putAtBottomOfDeck(card: Card) {
        this.deck.push(card);
    }

    public putAtTopOfDeck(card: Card) {
        this.deck.unshift(card);
    }

    public shuffle() {
        let counter = this._deck.length;
        let cards = this._deck;

        while (counter > 0) {
            let index = Math.floor(Math.random() * counter);

            counter--;

            let temp = cards[counter];
            cards[counter] = cards[index];
            cards[index] = temp;
        }

        return cards;
    }
}
